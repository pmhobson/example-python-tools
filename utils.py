import pandas


def head_tail(df, N=5):
    """ Returns the top *and* botton _N_
    rows from a dataframe
    """
    if df.shape[0] <= (N * 2):
        return df
    return pandas.concat([df.head(N), df.tail(N)])
