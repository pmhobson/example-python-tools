
from string import ascii_letters
import pandas
import pytest
import pandas.testing as pdtest

import utils

@pytest.fixture
def example_data():
    return {
        "long": pandas.DataFrame({
            "numbers": range(15),
            "letters": ascii_letters[:15]
        }),
        "short": pandas.DataFrame({
            "numbers": range(8),
            "letters": ascii_letters[:8]
        }),
    }

@pytest.mark.parametrize(("key", "N", "should_be_same"), [
    ("long", 5, False),
    ("long", 10, True),
    ("short", 5, True)
])
def test_head_tail(example_data, key, N, should_be_same):
    result = utils.head_tail(example_data[key], N=N)
    if should_be_same:
        pdtest.assert_frame_equal(result, example_data[key])
    else:
        assert result.shape[0] == (N * 2)
